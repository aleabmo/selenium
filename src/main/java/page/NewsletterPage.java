package main.java.page;

import main.java.page.base.NoMenuPageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewsletterPage extends NoMenuPageObject {

    @FindBy(id="Email")
    WebElement email;

    @FindBy(className="field-validation-error")
    List<WebElement> errMsg;

    @FindBy(className="btn")
    WebElement btn;

    @FindBy(xpath="//label[contains(@for, 'acceptConditions-visualcheck')]/i")
    WebElement chbxAccept;

    @FindBy(xpath="//label[contains(@for, 'acceptConditionsLOPD-visualcheck')]")
    WebElement chbxLOPD;

    /**
     * Constructor of current class
     * @param driver Driver is gonna be everywhere, useful in order to perform actions. Probably gonna be abstracted.
     */
    public NewsletterPage(WebDriver driver){
    	super(driver);
    }

    /**
     * Method used for having a better flow on application. It allows to make call just using '.'
     * @param driver Driver that we are currently using
     * @return A new class of himself to be able to call other methods
     */
    public static NewsletterPage using(WebDriver driver){
        return new NewsletterPage(driver);
    }

    /**
     * Method that returns all the errors from input camps
     * @return A hasmap<String,String> containing Error Camp, Error Msg
     */
    public Map<String,String> getErrorMsg() {
        Map<String, String> result = new HashMap<>();
        for(WebElement aux: errMsg) {
            if(!aux.getText().equalsIgnoreCase("")) {
                result.put(aux.getAttribute("data-valmsg-for"), aux.getText());
            }
        }
        return result;
    }

    /**
     * Set email in order to get subscription
     * @param strEmail string containing email
     * @return return itself just to improve usability
     */
    public NewsletterPage setEmail(String strEmail){
        email.sendKeys(strEmail);
        return this;
    }

    /**
     * Method in order to click on first radio button
     * @return return itself just to improve usability
     */
    public NewsletterPage checkAccept(){
        chbxAccept.click();
        return this;
    }

    /**
     * Method in order to click on second radio button
     * @return return itself just to improve usability
     */
    public NewsletterPage checkLOPD(){
        chbxLOPD.click();
        return this;
    }

    /**
     * Method used to click on susribe method
     * @return return itself just to improve usability
     */
    public  NewsletterPage clickbtn() {
        btn.click();
        return this;
    }
}