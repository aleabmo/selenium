package main.java.page;

import main.java.page.base.MenuPageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends MenuPageObject {

    WebDriver driver;

    @FindBy(xpath="//table//tr[@class='heading3']")
    WebElement homePageUserName;

    private String url = "/";

    public HomePage(WebDriver driver){
        super(driver);
    }   

    //Get the User name from Home Page
    public String getHomePageDashboardUserName(){
    	return homePageUserName.getText();
    }
}