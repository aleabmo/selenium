package main.java.page.account;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountDataPage extends AccountPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(xpath="//a[contains(@href,'/User/Modify')]")
    WebElement btnModify;

    public AccountDataPage(WebDriver driver){
    	super(driver);
    }
    
    public AccountCommunicationPage.AccountDataModPage goToModify() {
    	btnModify.click();
    	return new AccountCommunicationPage.AccountDataModPage(driver);
    }
}
