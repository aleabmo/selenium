package main.java.page.account;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountPassPage extends AccountPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="PasswordToModify")
    WebElement inputActualPass;

    @FindBy(id="Password")
    WebElement inputNewPass;

    @FindBy(id="ConfirmPassword")
    WebElement inputConfirmPass;

    @FindBy(xpath="//button")
    WebElement btnPass;

    public AccountPassPage(WebDriver driver){
    	super(driver);
    }

    public static AccountPassPage using(WebDriver driver) {
        return new AccountPassPage(driver);
    }
    
    public AccountPassPage inputActualPass(String pass) {
    	inputActualPass.sendKeys(pass);
        return this;
    }

    public AccountPassPage inputNewPass(String pass) {
        inputNewPass.sendKeys(pass);
        return this;
    }

    public AccountPassPage inputConfirmPass(String pass) {
        inputConfirmPass.sendKeys(pass);
    	return this;
    }

    public AccountDataPage sendForm() {
    	btnPass.click();
    	return new AccountDataPage(driver);
    }

    /**
     * Method for changing account password
     * @param actualPass String with actual password
     * @param newPass String with new password you want to set
     */
    public void changePass(String actualPass, String newPass) {
        AccountPassPage.using(driver)
                .inputActualPass(actualPass)
                .inputNewPass(newPass)
                .inputConfirmPass(newPass)
                .sendForm();
    }

}
