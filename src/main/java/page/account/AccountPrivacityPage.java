package main.java.page.account;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountPrivacityPage extends AccountPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(xpath="//*[@id=\"LOPDAuthorization\"]")
    List<WebElement> btnRadio;

    @FindBy(xpath="/html/body/main/form/div/section/div[2]/button")
    WebElement btnSend;

    public AccountPrivacityPage(WebDriver driver){
    	super(driver);
    }
    
    /**
     * Method that tells if policy is accepted,
     * @return
     */
    public boolean isAccepted() {
    	if (btnRadio.get(0).getAttribute("checked").equalsIgnoreCase("True"))
    		return true;
    	else
    		return false;
    }
    
    /**
     * Method that changes current selection
     */
    public void changeSelection() {
    	if(isAccepted())
    		btnRadio.get(1).click();
    	else
    		btnRadio.get(0).click();
    }
    
    /**
     * Sumbit changes and returns to data page
     * @return
     */
    public AccountDataPage sendForm() {
    	btnSend.click();
    	return new AccountDataPage(driver);
    }
}
