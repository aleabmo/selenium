package main.java.page.account;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class AccountCommunicationPage extends AccountPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="myorders")
    WebElement ordersTab;

    public AccountCommunicationPage(WebDriver driver){
    	super(driver);
    }

    public static class AccountDataModPage extends AccountPage {

        /**
         * All WebElements are identified by @FindBy annotation
         */

        @FindBy(id="Name")
        WebElement inputName;

        @FindBy(id="CompleteSurname")
        WebElement inputSurname;

        @FindBy(id="BirthDate_DayOfBirth")
        WebElement inputDay;

        @FindBy(id="dropdownMenu1")
        WebElement btnMonth;

        @FindBy(xpath="//a[contains(@role, 'menuitem')]")
        List<WebElement> inputMonth;

        @FindBy(id="BirthDate_YearOfBirth")
        WebElement inputYear;

        @FindBy(name="Gender")
        List<WebElement> inputGender;

        @FindBy(id="Address_Street")
        WebElement inputAddress;

        @FindBy(id="Address_Number")
        WebElement inputNum;

        @FindBy(id="Address_Additional")
        WebElement inputOpcional;

        @FindBy(id="Address_City")
        WebElement inputCity;

        @FindBy(id="Address_PostalCode")
        WebElement inputCP;

        @FindBy(id="Address_Province")
        WebElement inputProv;

        @FindBy(id="Phone")
        WebElement inputPhone;

        @FindBy(xpath="//section[@id=\"register\"]/form/div[3]/div/button")
        WebElement btnValidate;

        public AccountDataModPage(WebDriver driver){
            super(driver);
        }

        //Funcio per tal de que quedi mes clar visualment com es fant les crides, tant fora com dins de la class.
        public static AccountDataModPage using(WebDriver driver) {
            return new AccountDataModPage(driver);
        }

        public AccountDataModPage inputName(String name) {
            inputName.clear();
            inputName.sendKeys(name);
            return this;
        }

        public AccountDataModPage inputSurname(String surname) {
            inputSurname.clear();
            inputSurname.sendKeys(surname);
            return this;
        }

        public AccountDataModPage inputDate(String date) {
            //Dividim la data DD-MM-YYYY
            String[] aux = date.split("-");
            //Introduim el dia
            inputDay.clear();
            inputDay.sendKeys(aux[0]);
            //Obrim el desplegable del mes
            btnMonth.click();
            //Introduim el mes de {1-12}, restem 1 per tal de pasar a 0-11
            inputMonth.get(Integer.parseInt(aux[1])-1).click();
            //Introduim el any
            inputYear.clear();
            inputYear.sendKeys(aux[2]);
            return this;
        }

        public AccountDataModPage inputGender(char gender) {
            if(gender == 'M') inputGender.get(0).click();
            else inputGender.get(1).click();
            return this;
        }

        public AccountDataModPage inputAddress(String address) {
            inputAddress.clear();
            inputAddress.sendKeys(address);
            return this;
        }

        public AccountDataModPage inputNum(String num) {
            inputNum.clear();
            inputNum.sendKeys(num);
            return this;
        }

        public AccountDataModPage inputOpcional(String opt) {
            inputOpcional.clear();
            inputOpcional.sendKeys(opt);
            return this;
        }

        public AccountDataModPage inputCity(String city) {
            inputCity.clear();
            inputCity.sendKeys(city);
            return this;
        }

        public AccountDataModPage inputCP(String cp) {
            inputCP.clear();
            inputCP.sendKeys(cp);
            return this;
        }

        public AccountDataModPage inputProv(String prov) {
            inputProv.clear();
            inputProv.sendKeys(prov);
            return this;
        }

        public AccountDataModPage inputPhone(String phone) {
            inputPhone.clear();
            inputPhone.sendKeys(phone);
            return this;
        }

        public AccountDataPage clickValidate() {
            btnValidate.click();
            return new AccountDataPage(driver);
        }

        //Abstraccio per tal de que sigui mes facil
        public AccountDataPage inputData(String address, String num, String opt, String city, String cp, String prov, String phone) {
            AccountDataModPage.using(driver)
                    .inputAddress(address)
                    .inputNum(num)
                    .inputOpcional(opt)
                    .inputCity(city)
                    .inputCP(cp)
                    .inputProv(prov)
                    .inputPhone(phone);
            return clickValidate();
        }
    }
}
