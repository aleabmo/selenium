package main.java.page.base;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public abstract class PageObject {

	@FindBy(id="rgpdCookies")
	WebElement cookies;

	@FindBy(id="")
	WebElement btnCloseCookies;

	@FindBy(id="")
	WebElement btnAcceptCookies;

	protected WebDriver driver;
//	private static Logger log = Logger.getLogger(PageObject.class);

	public PageObject(WebDriver driver) {
		this.driver = driver;
        PageFactory.initElements(driver, this);
//        log.info("Creating pageobject");
	}
	
	/**
	 * Method to get current url in page
	 * @return String containing url of current page
	 */
	public String getUrl() {
		return driver.getCurrentUrl();
	}
	
	/**
	 * Method to get current web title
	 * @return String with the title
	 */
	public String getTitle() {
		return driver.getTitle();
	}

	public WebDriver getDriver() { return driver; }

	/**
	 * Function to close cookies layer
	 */
	public void closeCookies() {
	    try {
			btnCloseCookies.click();
		}catch (Exception e){
	    	System.out.println("No cookies");
		}
	}

	/**
	 * Function to acceptcookies layer
	 */
	public void acceptCookies() {
		try {
			btnAcceptCookies.click();
		}catch (Exception e){
			System.out.println("No cookies");
		}
	}

}
