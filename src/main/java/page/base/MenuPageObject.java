package main.java.page.base;

import main.java.page.*;
import main.java.page.account.AccountDataPage;
import main.java.page.cart.CartPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import main.java.exception.AlreadyLoggedException;
import main.java.exception.NotLoggedException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Constants;

public abstract class MenuPageObject extends PageObject {


    @FindBy(id="logoHeader")
    WebElement logo;

    @FindBy(id="input-search")
    WebElement search;

    @FindBy(xpath="//*[@id=\"searcher\"]/form/span[1]/button")
    WebElement btnSearch;

    @FindBy(xpath="/html/body/header/div[3]/div[1]/div[4]/span")
    WebElement btnCatalogo;

    @FindBy(xpath="/html/body/header/div[3]/div[1]/div[3]/ul/li[1]/a")
    WebElement world;
    
    @FindBy(xpath="//*[@id=\"camera\"]/a")
    WebElement syte;

    @FindBy(xpath="//*[@id=\"helpHeader\"]/span")
    WebElement help;

    @FindBy(xpath="//*[@id=\"WishMenu\"]/span")
    WebElement wish;

    @FindBy(className="userZoneName")
    WebElement userZoneName;

    @FindBy(xpath="//*[@id=\"userZone\"]/ul/li[1]/a")
    WebElement account;

    @FindBy(xpath="//*[@id=\"userZone\"]/ul/li[2]/a")
    WebElement logout;

    @FindBy(xpath="//*[@id=\"userZone\"]/span")
    WebElement login;

    @FindBy(xpath="//*[@id=\"cartZone\"]/span")
    WebElement cart;

    @FindBy(xpath="//*[@id=\"cartZone\"]/span/sup/strong")
    WebElement numCart;

    /**
     * Constructor
     * @param driver WebDriver in order to interactuate with site.
     */
    public MenuPageObject(WebDriver driver) {
    	super(driver);
    }
    
    /**
     * Method for going to homepage
     * @return HomePage in order to access new methods.
     */
    public HomePage clickLogo() {
    	logo.click();
    	return new HomePage(driver);
    }

    /**
     * Method for switching to other sites.
     * @return WorldPage in order to access new methods
     */
    public WorldPage clickWorld() {
    	world.click();
    	return new WorldPage(driver);
    }

    /**
     * Method for going to syte page
     * @return NewPage in order to acces new methods.
     */
    public SytePage clickNew() {
    	syte.click();
    	return new SytePage(driver);
    }

    /**
     * Method for going to Help page
     * @return HelpPage in order to access new methods.
     */
    public HelpPage clickHelp() {
    	help.click();
    	return new HelpPage(driver);
    }

    /**
     * Method for going to wishlist
     * @return WishPage in order to access new methods
     */
    public WishPage clickWishList() {
    	wish.click();
    	return new WishPage(driver);
    }

    /**
     * Method for going login page
     * @return LoginPage in order to access methods.
     * @throws AlreadyLoggedException Exception if you are already logged in
     */
    public LoginPage clickLogin() throws AlreadyLoggedException {
    	if(!userZoneName.getText().equals("LOGIN"))
    		throw new AlreadyLoggedException("You are already logged in");
    	login.click();
    	return new LoginPage(driver);
    }
    
    /**
     * Method for going to account information
     * @return AccountDataPage in order to aces new methods
     * @throws NotLoggedException Exception thrown if you are not logged.
     */
    public AccountDataPage goToAccount() throws NotLoggedException {
    	if(userZoneName.getText().equals("LOGIN"))
    		throw new NotLoggedException("Try to log before accessing account");
    	login.click();
    	account.click();
    	return new AccountDataPage(driver);
    }

    /**
     * Method for logging out
     * @return HomePage in order to access new methods
     * @throws NotLoggedException Exception thrown if you are not logged.
     */
    public HomePage logout() throws NotLoggedException {
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.visibilityOf(userZoneName));

    	if(userZoneName.getText().equals("LOGIN"))
    		throw new NotLoggedException("Try to log before accessing account");
    	login.click();
    	try{
            logout.click();
            System.out.println("WEA");
        }catch (Exception e){

        }
    	return new HomePage(driver);
    }

    /**
     * Method for got to cart
     * @return CartPage in order to acces new Methods
     */
    public CartPage clickCart() {
    	cart.click();
    	return new CartPage(driver);
    }

    /**
     * Method for retrieving number of items in cart
     * @return String with number of items shown on site
     */
    public String getNumCart() {
    	return numCart.getText();
    }
    
    /**
     * Method for searching elements
     * @param input String to search
     * @return PDPPage with the items searched.
     */
    public PDPPage search(String input) {
    	search.sendKeys(input);
    	btnSearch.click();
    	return new PDPPage(driver);
    }
    
    public CartPage goToCatalogo() {
    	btnCatalogo.click();
    	return new CartPage(driver);
    }
}
