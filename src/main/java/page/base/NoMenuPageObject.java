package main.java.page.base;

import main.java.page.HelpPage;
import main.java.page.HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public abstract class NoMenuPageObject extends PageObject {

    @FindBy(id="logoHeader")
    WebElement logo;

    @FindBy(id="//*[@id=\"help\"]/span")
    WebElement help;

	public NoMenuPageObject(WebDriver driver) {
		super(driver);
	}
	
	public HomePage clickLogo() {
		logo.click();
		return new HomePage(driver);
	}
	
	public HelpPage clickHelp() {
		help.click();
		return new HelpPage(driver);
	}
}
