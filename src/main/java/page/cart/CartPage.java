package main.java.page.cart;

import java.util.ArrayList;
import java.util.List;

import main.java.page.HomePage;
import main.java.page.PLPPage;
import main.java.page.base.MenuPageObject;
import main.java.page.checkout.CheckoutDeliveryPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CartPage extends MenuPageObject {

    @FindBy(xpath="//*[@id=\"productDetails\"]/div/table")
    WebElement productList;

    @FindBy(id="lineaCaja")
    WebElement boxLine;
    
    @FindBy(id="recommendedProducts")
    WebElement recommendedProducts;

    @FindBy(id="promotionsToChoose")
    WebElement carrouselPromo;

    @FindBy(name="advantageCode")
    WebElement inputPromo;

    @FindBy(xpath="//button[@id=\"promotionsToChoose\"]/section[2]/div/div[2]/div/form/div/span/button")
    WebElement btnApplyPromo;

    @FindBy(xpath="/html/body/main/div[2]/h1/strong")
    WebElement itemCount;

    @FindBy(xpath="//*[@id=\"summaryCart\"]/aside/div/div[1]/ul[1]/li[2]")
    WebElement itemscost;

    @FindBy(xpath="//*[@id=\"summaryCart\"]/aside/div/div[1]/ul[1]/li[4]")
    WebElement deliverycost;

    @FindBy(xpath="//*[@id=\"summaryCart\"]/aside/div/div[1]/ul[2]/li[2]")
    WebElement totalcost;

    @FindBy(xpath="/html/body/main/div[7]/a")
    WebElement btnShop;

//    @FindBy(xpath="//*[@id=\"summaryCart\"]/aside/div/div[2]/a")
//    WebElement btnCheckout;

    @FindBy(xpath="//a[contains(@href, '/Cart/Buy')]/div")
    WebElement btnCheckout;

    @FindBys(@FindBy(xpath="//*[@id=\"productDetails\"]/div/table/tbody/tr"))
    List<WebElement> listProduct;
    
    Product product;
    private String url = "/cart";

    public CartPage(WebDriver driver) {
		super(driver);
	}
    
    /**
     * Method for listing all promtions available
     * @return List of Strings containing all promo names.
     */
    public List<String> listPromo(){
    	List<String> result = new ArrayList<>();
    	return result;
    }
   
    /**
     * Method for selecting a carrousel promo
     */
    public void selectPromo() {
    	
    }
    
    /**
     * Method to apply own code promotion
     * @param code String with code of promotion to be applied
     */
    public void applyPromo(String code) {
    	inputPromo.sendKeys(code);
    	btnApplyPromo.click();
    }
    
    /**
     * Method for going from cart to homepage
     * @return
     */
    public HomePage goToShop() {
    	return new HomePage(driver);
    }
    
    /**
     * Method for going to checkout page
     * @return a page object to handle checkout
     */
    public CheckoutDeliveryPage goToCheckout(){
    	/* Los driver wait creo que no son necesarios pero no estan de mas, por si el carrito hay mucho item y se queda el modal*/
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.and(

        ExpectedConditions.invisibilityOfElementLocated(By.id("myModal")),
        ExpectedConditions.invisibilityOfElementLocated(By.id("myModalWait")),
        ExpectedConditions.elementToBeClickable(btnCheckout)
        ));

    	btnCheckout.click();
    	return new CheckoutDeliveryPage(driver);
    }
    
    /**
     * Get the number of items which appears at the top of the cartpage
     * @return String with the number of items in the page
     */
    public String getNumItems() {
    	return itemCount.getText();
    }
    
    /**
     * Get the number of items on the cart
     * @return the calculated number of items in the page
     */
    public int getProductCount() {
    	return listProduct.size();
    }
    
    /**
     * Select a product from the cart
     * @param index of the product you wanna select
     * @return the product as an Object itself
     */
    public Product selectProduct(int index) {
    	product = new Product(listProduct.get(index));
    	return product;
    }
    
    /**
     * Function to get the total cost of items
     * @return cost of items added up
     */
    public String getTotalItems() {
    	return itemscost.getText();
    }

    /**
     * Function return total delivery cost
     * @return total delivery cost as string
     */
    public String getTotalDelivery() {
    	return deliverycost.getText();
    }

    /**
     * Function to get the total cost
     * @return return the total cost as string from webpage
     */
    public String getTotal() {
    	return totalcost.getText();
    }
    
    /**
     * Function to click "CONTINUAR COMPRANDO"
     * @return
     */
    public PLPPage goToShoping() {
    	btnShop.click();
    	return new PLPPage(driver);
    }
    
    /**
     * Class to atomize each singular promotion on carrousel 
     * @author aleixabengochea
     *
     */
    public class Promotion {
    	
    }
    
    /**
     * Class to atomize each singular item on boxLine
     * @author aleixabengochea
     *
     */
    public class BoxLineItem {
    	
    }
    
    /**
     * Class to atomize each product in current cart
     * @author aleixabengochea
     *
     */
    public class Product {

    	private WebElement tr;

    	public Product(WebElement tr) {
    		this.tr = tr;
    	}
    	
    	public String getImg() {
    		return tr.findElement(By.xpath("//td[1]/div/a/img")).getAttribute("src");
    	}
    	
    	public String getBrand() {
    		return tr.findElement(By.xpath("//td[1]/div/div/span[1]")).getText();
    	}
    	
    	public String getDesc() {
    		return tr.findElement(By.xpath("//td[1]/div/div/span[2]")).getText();
    	}
    	
    	public String getSeller() {
    		return tr.findElement(By.xpath("//td[1]/div/div/span[3]")).getText();
    	}
    	
    	public String getRef() {
    		return tr.findElement(By.xpath("//td[1]/div/div/span[5]")).getText();
    	}
    	
    	public String getAvailability() {
    		return tr.findElement(By.xpath("//td[1]/div/div/span[6]")).getText();
    	}
    	
    	public void addToWish() {
    		tr.findElement(By.id("guardarFavorito1")).click();
    	}
    	
    	public String getColor() {
    		return tr.findElement(By.xpath("//td[2]/ul/li[1]")).getText();
    	}
    	
    	public String getSize() {
    		return tr.findElement(By.xpath("//td[2]/ul/li[2]")).getText();
    	}

    	public String getQt() {
    		return tr.findElement(By.xpath("//td[2]/ul/li[3]")).getText();
    	}
    	
    	public void modifyProduct() {
    		tr.findElement(By.id("modifyButton1")).click();
    	}
    	
    	public String getPrice() {
    		return tr.findElement(By.xpath("//td[3]/div/div/span[1]")).getText() + "," + tr.findElement(By.xpath("//td[3]/div/div/span[2]")).getText();
    	}
    	
    	public void sendToTrash() {
    		tr.findElement(By.xpath("//td[3]/div/div[2]/a")).click();
    	}
    	
    	public String toString() {
    		return this.getImg()+'\n'+this.getBrand()+'\n'+this.getDesc()+'\n'+this.getSeller()+'\n'+this.getAvailability()+'\n'+this.getColor()+'\n'+this.getSize()+'\n'+this.getQt()+'\n'+this.getPrice();
    	}
    	
    }
}
