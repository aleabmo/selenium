package main.java.page.checkout;

import java.util.concurrent.TimeUnit;

import main.java.page.ThanksPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import main.java.exception.IsDisabledException;
import utils.Constants;

public class CheckoutPaymentPage extends CheckoutPage{

    @FindBy(css="a > .light")
	private WebElement btnChangeAddress;

    @FindBy(css=".col-sm-6 > div")
	private WebElement address;

    @FindBy(css=".col-sm-6 > .text-uppercase")
	private WebElement delivery;

    @FindBy(id="doOrder")
	private WebElement btnPayment;

    @FindBy(id="OrderMV_RefundNumber")
	private WebElement inputPromo;

    @FindBy(css=".btn-sm")
	private WebElement btnPromo;

    @FindBy(css=".voffset4 > .radio")
	private WebElement radioCard;

    @FindBy(css=".checkoutLogos > img")
	private WebElement radioPayPal;

    @FindBy(css="#heading-fpD .radio")
	private WebElement radioBank;

    /*
    @FindBy(css="#heading-fpR .radio")
    WebElement radioCash;
    */
    @FindBy(id="heading-fpR")
	private WebElement radioCash;

    @FindBy(id="OrderMV_PaymentMV_NIFNIEToValidate")
	private WebElement inputDni;

    @FindBy(id="numHabitual")
	private WebElement numHabCard;

    @FindBy(xpath="//fieldset[@id = 'permanentCreditCard']/div[2]/a")
	private WebElement btnOtherCard;
    
    
    @FindBy(id="pas_ccnum")
    private WebElement numCard;
    @FindBy(id="pas_expiry")
    private WebElement expCard;
    @FindBy(id="pas_cccvc")
	private WebElement CVVCard;
    @FindBy(id="pas_ccname")
	private WebElement nameCard;
    @FindBy(id="rxp-primary-btn")
	private WebElement btnCard;
    @FindBy(xpath="//*[@id=\"collapse-fpT\"]/div[1]/div[1]/label/span")
	private WebElement btnSaveCard;

    private String payment;

    /**
     * Constructor, setting card payment as default payment method
     * @param driver Current driver
     */
	public CheckoutPaymentPage(WebDriver driver) {
		super(driver);
		payment = "card";

		WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
		wait.until(ExpectedConditions.and(
				ExpectedConditions.attributeToBe(By.id("myModalWait"), "style", "display: block;")
				));
	}
	
	/**
	 * Function which needs to be abstracted in another class in order to make easier to pay with card
	 * @param numCard String with number of card
	 * @param expCard String with expiration date
	 * @param CVVCard String with cvv card
	 * @param nameCard String with name of card
	 * @param enable Boolean in order to save or not the card
	 * @throws IsDisabledException If you can not set card
	 */
	public void setCreditCard(String numCard, String expCard, String CVVCard, String nameCard, Boolean enable) throws IsDisabledException {
	    //ajaxWait(radioCard, Constants.TIMEOUT);
		//radioCard.click();
		payment = "card";

		setCardNum(numCard);
		setCardExp(expCard);
		setCardCVC(CVVCard);
		setCardName(nameCard);
		if (enable)
			this.setCardSave();
		driver.manage().timeouts().implicitlyWait(Constants.TIMEOUT, TimeUnit.SECONDS);	
	}

	//// Molt trist i cutre la manera de fer-ho. He passat massa estona fent aixo algun dia amb ment fresca faig refactor
	/// per a que no s'oblidi, el problema esta a l hora de posar la tarjeta, ja que efectua dues crides via ajax. A ver com 
	/// ho puc implementar, de moment re del que he fet funciona.
	/**
	 * Function which sets number of card
	 * @param numCard String wih card's number
	 */
	public void setCardNum(String numCard) {
		driver.switchTo().frame("hidden_iframe");
		this.numCard.sendKeys(numCard);
		//ajaxWait(driver.findElement(By.id("rpx-date-cccvc-section")), 5);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(!this.numCard.getText().equalsIgnoreCase(numCard))
			this.numCard.sendKeys(numCard);
		driver.switchTo().defaultContent();
	}

	/**
	 * Function which sets expiration date of card ("MM/DD")
	 * @param expCard String with day/month
	 */
	public void setCardExp(String expCard) {
		driver.switchTo().frame("hidden_iframe");
		this.expCard.sendKeys(expCard);
		driver.switchTo().defaultContent();
	}

	/**
	 * Function which sets CVC of card
	 * @param CVVCard String with card's CVC
	 */
	public void setCardCVC(String CVVCard) {
		driver.switchTo().frame("hidden_iframe");
		this.CVVCard.sendKeys(CVVCard);
		driver.switchTo().defaultContent();
	}

	/**
	 * Function which sets name of card holder
	 * @param nameCard String with card's name holder
	 */
	public void setCardName(String nameCard) {
		driver.switchTo().frame("hidden_iframe");
		this.nameCard.sendKeys(nameCard);
		driver.switchTo().defaultContent();
	}
	
	/**
	 * Function which click button in order to save credit card information
	 */
	public void setCardSave() {
		btnSaveCard.click();
	}
	
	/**
	 * Function to enter DNI when needed
	 * @param dni String with dni
	 */
	public void setDNI(String dni) {
		inputDni.sendKeys(dni);
		payment = "oldcard";

	}

	/**
	 * Return String with the saved card
	 * @return String with card's number
	 */
	public String getHabCard() {
		return numHabCard.getText();
	}
	
	/**
	 * Function to open modal in order to input new card
	 */
	public void clickOtherCard() {
		WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
		wait.until(ExpectedConditions.elementToBeClickable(btnOtherCard));
		btnOtherCard.click();
	}
	
	/**
	 * Function which selects paypal payment type
	 * @throws IsDisabledException
	 */
	public void setPayPal() throws IsDisabledException {
		WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
		wait.until(ExpectedConditions.elementToBeClickable(radioPayPal));
		radioPayPal.click();
		payment = "paypal";
	}

	/**
	 * Function which selects banc payment type
	 * @throws IsDisabledException
	 */
	public void setBank() throws IsDisabledException {
		WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("myModalWait")));
		wait.until(ExpectedConditions.elementToBeClickable(radioBank));
		radioBank.click();
		payment = "bank";
	}
	
	/**
	 * function which selects cash payment type
	 * @throws IsDisabledException
	 */
	public void setCash() throws IsDisabledException {
		WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);

		wait.until(ExpectedConditions.and(
				ExpectedConditions.attributeToBe(By.id("myModalWait"), "style", "display: none;"),
				ExpectedConditions.invisibilityOfElementLocated(By.className("modal-backdrop")),
				//ExpectedConditions.visibilityOf(radioCash),
				ExpectedConditions.elementToBeClickable(radioCash)
				));

		radioCash.click();

		wait.until(ExpectedConditions.and(
				//Intentar fer un ajaxwait amb el contingut del contrareembolso
				// El error es la coma, es per a marcar-ho
				ExpectedConditions.attributeToBe(By.id("myModalWait"), "style", "display: block;")
				));

		payment = "cash";
	}
	
	/**
	 * Function which writes promotion on cart input
	 * @param promo String with promo to apply
	 */
	public void setPromotion(String promo) {
		inputPromo.sendKeys(promo);
	}
	
	/**
	 * Function which click apply promotion to cart
	 */
	public void applyPromotion() {
		btnPromo.click();
	}
	
	/**
	 * Method for getting delivery choosen
	 * @return delivery option as String
	 */
	public String getDelivery() {
		return delivery.getText();
	}

	/**
	 * Method for returning current address
	 * @return Delivery address as String
	 */
	public String getAddress() {
		return address.getText();
	}
	
	/**
	 * Return to delivery page
	 * @return delivery page in order to acces methods
	 */
	public CheckoutDeliveryPage goToDelivery() {
		return new CheckoutDeliveryPage(driver);
	}
	
	/**
	 * If correct it sends you to thankyou page
	 * @return Thankyou Page in order to acces new methods
	 */
	public ThanksPage goToFinishPayment(){
		if (payment.equals("card")) {
			driver.switchTo().frame("hidden_iframe");
			/*
			WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
			wait.until(ExpectedConditions.textToBePresentInElementValue(this.numCard, numCard));
			wait.until(ExpectedConditions.textToBePresentInElementValue(this.expCard, expCard));
			wait.until(ExpectedConditions.textToBePresentInElementValue(this.CVVCard, CVVCard));
			wait.until(ExpectedConditions.textToBePresentInElementValue(this.nameCard, nameCard));
			*/
			btnCard.click();
			btnCard.click();
			driver.switchTo().defaultContent();
		}
		else {
			WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);

			wait.until(ExpectedConditions.and(
//					ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@class, 'modal-backdrop fade in')]")),
					ExpectedConditions.attributeToBe(By.id("myModalWait"), "style", "display: none;"),
//					ExpectedConditions.visibilityOf(btnPayment),
					ExpectedConditions.elementToBeClickable(btnPayment)
					));

			btnPayment.click();
		}
		return new ThanksPage(driver);
	}

}
