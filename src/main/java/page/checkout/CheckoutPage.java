package main.java.page.checkout;

import main.java.page.base.NoMenuPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public abstract class CheckoutPage extends NoMenuPageObject {

    @FindBy(xpath="//*[@id=\"asideChk\"]/div/div[1]/span[1]/strong")
    WebElement name;

    @FindBy(xpath="//*[@id=\"asideChk\"]/div/div[1]/span[2]")
    WebElement email;

    @FindBy(id="checkoutSummary")
    WebElement pricesum;


	public CheckoutPage(WebDriver driver) {
		
		super(driver);
	}

	/**
	 * Returns name of current user
	 * @return String with name
	 */
	public String getName() {
		return name.getText();
	}
	
	/**
	 * Returns email of current user
	 * @return String with email
	 */
	public String getEmail(){
		return email.getText();
	}

// Method's in order to get the price on the right side.

	/**
	 * Returns price of total products
	 * @return String with price
	 */
	public String getItemsPrice() {
		return pricesum.findElement(By.xpath("//ul/li[2]/span[1]")).getText() + pricesum.findElement(By.xpath("//ul/li[2]/span[2]")).getText();
	}
	
	/**
	 * Return delivery costs
	 * @return String with price 
	 */
	public String getDeliveryPrice() {
		return pricesum.findElement(By.xpath("//ul/li[4]/span[1]")).getText() + pricesum.findElement(By.xpath("//ul/li[4]/span[2]")).getText();
	}
	
	/**
	 * Returns expenses of type of payment
	 * @return String with price
	 */
	public String getPaymentPrice() {
		return pricesum.findElement(By.xpath("//ul/li[6]/span[1]")).getText() + pricesum.findElement(By.xpath("//ul/li[6]/span[2]")).getText();
	}

	/**
	 * Return price of total expenses
	 * @return String with price
	 */
	public String getTotalPrice() {
		return pricesum.findElement(By.xpath("//ul/li[8]/span[1]")).getText() + pricesum.findElement(By.xpath("//ul/li[8]/span[2]")).getText();
	}


}
