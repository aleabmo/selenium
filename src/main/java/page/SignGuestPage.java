package main.java.page;

import main.java.page.base.NoMenuPageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignGuestPage extends NoMenuPageObject {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="Email")
    WebElement inputEmail;

    @FindBy(id="Name")
    WebElement inputName;

    @FindBy(id="CompleteSurname")
    WebElement inputSurname;

    @FindBy(xpath="//*[@id=\"register\"]/div/form/div[2]/div/button")
    WebElement btnSignIn;

    public SignGuestPage(WebDriver driver){
    	super(driver);
    }

    /**
     * Method to insert email
     * @param strEmail String containing email
     */
    public void setEmail(String strEmail){
        inputEmail.sendKeys(strEmail);     
    }

    /**
     * Method to insert name
     * @param strName String containing name
     */
    public void setName(String strName){
        inputName.sendKeys(strName);     
    }

    /**
     * Method to insert surname
     * @param strSurname String containing surname
     */
    public void setSurname(String strSurname){
    	inputSurname.sendKeys(strSurname);
    }

    /**
     * Method to sumbit form
     */
    public void clickSignIn(){
            btnSignIn.click();
    }  

    /**
     * Method to login on webpage as guestuser
     * @param strEmail Email of account
     * @param strName Name of account
     * @param strSurname Surname of account
     * @return
     */
    public HomePage signAsGuest(String strEmail, String strName, String strSurname){
        this.setEmail(strEmail);
        this.setName(strName);
        this.setSurname(strSurname);
        this.clickSignIn();           
        return new HomePage(driver);

    }

}