package main.java.page.modal;

import main.java.page.base.PageObject;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Constants;

public class GlobaleModal  extends PageObject {

    @FindBy(id="gle_selectedCountry")
    WebElement selectCountry;

    @FindBy(className="glCancelBtn")
    WebElement saveBtn;

    public GlobaleModal(WebDriver driver){
        super(driver);
    }

    public void chooseCountry(String country){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        try {
            wait.until(ExpectedConditions.and(
                    ExpectedConditions.visibilityOf(driver.findElement(By.id("globale_overlay"))),
                    ExpectedConditions.visibilityOf(driver.findElement(By.id("globalePopupWrapper")))
            ));
        } catch (TimeoutException e) {
            System.out.println("Timeout");
            System.out.println(e);
            System.out.println(driver.getPageSource());

        } catch(NoSuchElementException e) {
            System.out.println("Element not found");
            System.out.println(e);
            System.out.println(driver.getPageSource());
        }
        Select select = new Select(selectCountry);
        //select.selectByVisibleText(country);
        select.selectByValue(country);
        saveBtn.click();
        try{
            wait.until(ExpectedConditions.and(
                    ExpectedConditions.invisibilityOf(driver.findElement(By.id("globale_overlay"))),
                    ExpectedConditions.invisibilityOf(driver.findElement(By.id("globalePopupWrapper")))
            ));
        } catch (TimeoutException e) {
            System.out.println("Timeout");
            System.out.println(e);
            System.out.println(driver.getPageSource());
        } catch (NoSuchElementException e){
            System.out.println("Element not found");
            System.out.println(e);
            System.out.println(driver.getPageSource());
        }
    }

}
