package main.java.exception;

public class  AlreadyLoggedException extends Exception {
	private static final long serialVersionUID = 1L;

	//Exception for no product on PDPPage
	public AlreadyLoggedException(String errMsg) {
		super(errMsg);
	}
}