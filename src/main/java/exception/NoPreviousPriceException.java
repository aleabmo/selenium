package main.java.exception;

public class  NoPreviousPriceException extends Exception {
	private static final long serialVersionUID = 1L;

	//Exception for no product on PDPPage
	public NoPreviousPriceException(String errMsg) {
		super(errMsg);
	}
}