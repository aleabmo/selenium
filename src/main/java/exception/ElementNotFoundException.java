package main.java.exception;

public class  ElementNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	//Exception for no product on PDPPage
	public ElementNotFoundException(String errMsg) {
		super(errMsg);
	}
}