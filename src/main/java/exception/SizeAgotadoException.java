package main.java.exception;

public class  SizeAgotadoException extends Exception {
	private static final long serialVersionUID = 1L;

	//Exception for no product on PDPPage
	public SizeAgotadoException(String errMsg) {
		super(errMsg);
	}
}