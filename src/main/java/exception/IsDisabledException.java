package main.java.exception;

public class  IsDisabledException extends Exception {
	private static final long serialVersionUID = 1L;

	//Exception for no product on PDPPage
	public IsDisabledException(String errMsg) {
		super(errMsg);
	}
}