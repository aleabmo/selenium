package main.java.exception;

public class  NoMoreOrdersException extends Exception {
    private static final long serialVersionUID = 1L;

    //Exception for no product on PDPPage
    public NoMoreOrdersException(String errMsg) {
        super(errMsg);
    }
}
