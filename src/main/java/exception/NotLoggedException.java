package main.java.exception;

public class NotLoggedException extends Exception {
	private static final long serialVersionUID = 1L;

	//Exception for no product on PDPPage
	public NotLoggedException(String errMsg) {
		super(errMsg);
	}
}