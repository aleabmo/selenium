package main.java.exception;

public class  NoRatingException extends Exception {
	private static final long serialVersionUID = 1L;

	//Exception for no product on PDPPage
	public NoRatingException(String errMsg) {
		super(errMsg);
	}
}