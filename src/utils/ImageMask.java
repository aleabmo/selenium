package utils;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ImageMask {
    private static Color color = Color.yellow;
    private List<Square> llista;

    ImageMask(){
        llista = new ArrayList<>();
    }

    public ImageMask(String filePath){
        llista = new ArrayList<>();
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(filePath)){
            Object obj = jsonParser.parse(reader);
            JSONArray squareList = (JSONArray) obj;
            squareList.forEach( square -> parseSquareObject( (JSONObject) square ) );
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }catch (ParseException e){
            e.printStackTrace();
        }
    }

    private void parseSquareObject(JSONObject square){
        JSONObject squareObject = (JSONObject) square.get("square");
        Long x = (Long) squareObject.get("x");
        Long y = (Long) squareObject.get("y");
        Long x2 = (Long) squareObject.get("x2");
        Long y2 = (Long) squareObject.get("y2");
        addSquare(new Square(x.intValue(), y.intValue(),x2.intValue(),y2.intValue()));
    }

public static void applyMask(String fileWithPath, ImageMask mask){
    try {
        File outfile = new File(fileWithPath);
        BufferedImage img = ImageIO.read(outfile);
        Graphics2D g2d = img.createGraphics();
        g2d.setColor(color);
        g2d.setStroke(new BasicStroke(10));
        for(Square aux : mask.getSquareList())
            g2d.fillRect(aux.getPX(), aux.getPY(), aux.getPW(), aux.getPH());
        g2d.dispose();
        ImageIO.write(img, "png", outfile);
        System.out.println("Aplicat");
    } catch (IOException e) {
        e.printStackTrace();
    }
}

void applyMask(String fileWithPath){
    try {
        File outfile = new File(fileWithPath);
        BufferedImage img = ImageIO.read(outfile);
        Graphics2D g2d = img.createGraphics();
        g2d.setColor(color);
        g2d.setStroke(new BasicStroke(10));
        for(Square aux : llista)
            g2d.fillRect(aux.getPX(), aux.getPY(), aux.getPW(), aux.getPH());
        g2d.dispose();
        ImageIO.write(img, "png", outfile);
        System.out.println("Disposeed");
    } catch (IOException e) {
        e.printStackTrace();
    }
}

    void addSquare(Square aux){
        llista.add(aux);
    }

    List<Square> getSquareList(){
        return llista;
    }

    void deleteSquare(int x, int y){
        Square wea = null;
        boolean todo = false;
        for(Square aux : llista){
            if (aux.contains(x,y)){
                wea = aux;
                todo = true;
                break;
            }
        }
        if(todo)
            llista.remove(wea);
    }

    public static void setColor(Color color){
        ImageMask.color = color;
    }

    /**
     * Method in order to return if there is square with this point
     * @param x x of point
     * @param y y of point
     * @return boolean if at least one square is in.
     */
    public boolean contains(int x, int y){
        for(Square aux : llista){
            if (aux.contains(x,y))
                return true;
        }
        return false;
    }

    /**
     * Method used to save current mask
     * @param filePath File where you want to save json.
     */
    public void saveJson(String filePath){
        JSONObject squarePoints;
        JSONObject squareObject;
        JSONArray squareList = new JSONArray();
        for(Square aux : llista){
            squarePoints = new JSONObject();
            squareObject = new JSONObject();
            squarePoints.put("x", aux.x);
            squarePoints.put("y", aux.y);
            squarePoints.put("x2", aux.x2);
            squarePoints.put("y2", aux.y2);
            squareObject.put("square", squarePoints);
            squareList.add(squareObject);
        }
        try( FileWriter file = new FileWriter(filePath) ){
            file.write(squareList.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        String result = "";
        for (Square aux : llista)
            result += aux.x+":"+aux.y+"/"+aux.x2+":"+aux.y2 +"\n";
        return result;
    }
}
