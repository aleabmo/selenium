package utils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImagePanel extends JPanel {
    private BufferedImage image;
    private int x, y, x2, y2;
    ImageMask mask = new ImageMask();
    private Boolean mod=false;

    ImagePanel(String path){
        try {
            image = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ImageMouseListener listener = new ImageMouseListener();
        addMouseListener(listener);
        addMouseMotionListener(listener);
    }

    class ImageMouseListener extends MouseAdapter{
        @Override
        public void mousePressed(MouseEvent e) {
            if(!mask.contains(e.getX(), e.getY())){
                setStartPoint(e.getX(), e.getY());
                mod = true;
            }
            else mask.deleteSquare(e.getX(),  e.getY());
            repaint();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if(mod){
                setEndPoint(e.getX(), e.getY());
                repaint();
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if(mod){
                setEndPoint(e.getX(),e.getY());
                newSquare();
            }
            mod = false;
            repaint();
        }
    }

    private void setStartPoint(int x, int y){
        this.x = x;
        this.y = y;
    }

    private void setEndPoint(int x, int y){
        this.x2 = x;
        this.y2 = y;
    }

    private void newSquare(){
        if (Math.abs(x-x2)>10)
            mask.addSquare(new Square(x, y, x2, y2));
    }

    private void drawPerfectRect(Graphics g, int x, int y, int x2, int y2){
        int px = Math.min(x,x2);
        int py = Math.min(y,y2);
        int pw = Math.abs(x-x2);
        int ph = Math.abs(y-y2);
        g.drawRect(px, py, pw, ph);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0 ,0 ,this);
        for (Square aux : mask.getSquareList())
            aux.paintComponent(g);
        if(mod)
            drawPerfectRect(g, x, y, x2, y2);
    }
}
