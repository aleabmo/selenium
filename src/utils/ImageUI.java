package utils;

import javax.swing.*;
import java.awt.*;

public class ImageUI extends JFrame {

    private JMenuBar menuBar;
    private ImagePanel panel;
    private String filePath = "C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\pre_help.png";
    private String filePathJson = "C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\pre_help.json";

    private ImageUI(){
        buildMenu();
        initComponents();
    }

    private void initComponents(){
        this.setJMenuBar(menuBar);

        panel = new ImagePanel(filePath);
        this.setContentPane(panel);

        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
    }

    private void buildMenu(){
        menuBar = new JMenuBar();
        JMenu menu = new JMenu("Mask");
        JMenuItem menuItem = new JMenuItem("Save");

        menuBar.add(menu);
        menu.add(menuItem);
        //menuItem.addActionListener(e -> panel.mask.applyMask(filePath));
        menuItem.addActionListener(e -> panel.mask.saveJson(filePathJson));
    }

    public static void main(String[] args){
        EventQueue.invokeLater(() -> new ImageUI().setVisible(true));
    }
}
