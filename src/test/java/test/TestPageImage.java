package test.java.test;

import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ComparisonResult;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import main.java.page.account.AccountOrdersPage;
import main.java.page.LoginPage;
import org.testng.annotations.Test;
import utils.Constants;
import utils.TakeScreen;

public class TestPageImage extends BaseTest {

    String filePath = "C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\img\\";
    String filePathPre = "C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\img\\pre_";
    String filePathPro = "C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\img\\pro_";

    /*
    WebDriver driver;
    //String filePath = "C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\img\\";
    //String filePathPre = "C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\img\\pre_";
    //String filePathPro = "C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\img\\pro_";
    String filePath = "/Users/aleixabengochea/lel/";
    String filePathPre = "/Users/aleixabengochea/lel/pre_";
    String filePathPro = "/Users/aleixabengochea/lel/pro_";
    */

    /*
    String filePath = "/Users/aleixabengochea/lel/";
    String filePathPre = "/Users/aleixabengochea/lel/pre_";
    String filePathPro = "/Users/aleixabengochea/lel/pro_";
    */

    /*
    @BeforeTest
<<<<<<< HEAD:src/test/java/test/TestPageImage.java
    public void setup() {

        // WIN
        System.setProperty("webdriver.chrome.driver","C:\\Users\\AleixAbengochea\\Downloads\\chromedriver_win33\\chromedriver.exe");
        driver = new ChromeDriver();

        /* GRID
        ChromeOptions options = new ChromeOptions();
        DesiredCapabilities dc = DesiredCapabilities.chrome();
        dc.setBrowserName("chrome");
        driver = new RemoteWebDriver(new URL("http://localhost:5555/wd/hub"), dc);
=======
    public void setup() throws MalformedURLException {
        //System.setProperty("webdriver.chrome.driver","C:\\Users\\AleixAbengochea\\Downloads\\chromedriver_win33\\chromedriver.exe");

        //  System.setProperty("webdriver.chrome.driver","/usr/local/bin/chromoedriver");
        ChromeOptions options = new ChromeOptions();
        DesiredCapabilities dc = DesiredCapabilities.chrome();
        //dc.setCapability(ChromeOptions.CAPABILITY, options);
        dc.setBrowserName("chrome");
        driver = new RemoteWebDriver(new URL("http://localhost:5555/wd/hub"), dc);
        //driver = new ChromeDriver();
>>>>>>> 6026499e13621c99bdb8c0fc4fa76f2536452234:src/test/TestPageImage.java
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
    */

    /**
     * This test go to https://pre.venca.es
     * Testing if taking screen is working properly.
     * Generic testing
     * @throws Exception
     */
    @Test(priority=1)
    public void test_Home_Page_Appear_Correct() throws Exception {

        driver.get(Constants.ENV+"/login");
        LoginPage objLogin = new LoginPage(driver);
        objLogin.loginToVenca("aleabmo@gmail.com", "123456");
        driver.get(Constants.ENV+"/mis-pedidos");
        AccountOrdersPage wea = new AccountOrdersPage(driver);
        wea.countOrders();

        TakeScreen.takeSnapShot(driver, filePathPre+"pedidos.png") ;

        /*
        driver.get("https://pre.venca.es/help");
       // new HelpPage(driver);
        TakeScreen.takeSnapShot(driver, filePathPre+"help.png") ;

        driver.get("https://pre.venca.es/cart");
        //new CartPage(driver);
        TakeScreen.takeSnapShot(driver, filePathPre+"cart.png") ;

        driver.get("https://pre.venca.es/e/38");
        //new PLPPage(driver);
        TakeScreen.takeSnapShot(driver, filePathPre+"plp.png") ;

        driver.get("https://pre.venca.es/p/112004");
        //new PDPPage(driver);
        TakeScreen.takeSnapShot(driver, filePathPre+"pdp.png") ;

        driver.get("https://pre.venca.es/WishList");
        //new WishPage(driver);
        TakeScreen.takeSnapShot(driver, filePathPre+"wish.png") ;

*/
        ////// PROOO
        driver.get("https://test.venca.es/login");
        objLogin = new LoginPage(driver);
        objLogin.loginToVenca("", "123456");
        driver.get("https://test.venca.es/mis-pedidos");
        wea = new AccountOrdersPage(driver);
        wea.countOrders();
        //new HomePage(driver);
        TakeScreen.takeSnapShot(driver, filePathPro+"pedidos.png") ;

        /*
        driver.get("https://www.venca.es/help");
        //new HelpPage(driver);
        TakeScreen.takeSnapShot(driver, filePathPro+"help.png") ;

        driver.get("https://www.venca.es/cart");
        //new CartPage(driver);
        TakeScreen.takeSnapShot(driver, filePathPro+"cart.png") ;

        driver.get("https://www.venca.es/e/38");
        //new PLPPage(driver);
        TakeScreen.takeSnapShot(driver, filePathPro+"plp.png") ;

        driver.get("https://www.venca.es/p/112004");
        //new PDPPage(driver);
        TakeScreen.takeSnapShot(driver, filePathPro+"pdp.png") ;

        driver.get("https://www.venca.es/WishList");
        //new WishPage(driver);
        TakeScreen.takeSnapShot(driver, filePathPro+"wish.png") ;

         */

        driver.close();

        compareImg("pedidos");
        /*
        compareImg("help");
        compareImg("wish");
        compareImg("plp");
        compareImg("pdp");
        compareImg("cart");
         */
    }
    private void compareImg(String href) throws IOException {
        BufferedImage expectedImage = ImageComparisonUtil.readImageFromFile(new File(filePathPre+href+".png"));
        BufferedImage actualImage = ImageComparisonUtil.readImageFromFile(new File(filePathPro+href+".png"));

        File resultDestination = new File( filePath+href+".png" );

        ImageComparison imageComparison = new ImageComparison( expectedImage, actualImage, resultDestination );
        imageComparison.setDestination(resultDestination);
        imageComparison.setPixelToleranceLevel(0.0);
        ComparisonResult imageComparisonResult = imageComparison.compareImages();
        BufferedImage resultImage = imageComparisonResult.getResult();
        ImageComparisonUtil.saveImage(resultDestination, resultImage);
    }
}
