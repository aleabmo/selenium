package test.java.test;

import main.java.exception.AlreadyLoggedException;
import main.java.exception.NotLoggedException;
import main.java.page.HomePage;
import main.java.page.LoginPage;
import main.java.page.RecoveryPage;
import main.java.page.RegisterPage;
import main.java.page.modal.GlobaleModal;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import utils.Constants;

public class RegisterTest extends BaseTest {
    HomePage objHome;
    RecoveryPage objRec;
    LoginPage objLogin;
    RegisterPage objRegister;
    GlobaleModal objModal;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     */

    @Test(priority=1)
    public void test_new_register() throws AlreadyLoggedException, NotLoggedException {
        driver.get(Constants.ENV);
        /*try {
            objModal = new GlobaleModal(driver);
            objModal.chooseCountry("ES");
        }catch (Exception e){
            System.out.println("No globale");
        }*/
        objHome = new HomePage(driver);
        objLogin = objHome.clickLogin();
        objRegister = objLogin.goToRegister();
        objHome = objRegister.registerToVenca("testingvenca1@gmail.com", "123456", "wea", "wea", "1", "Enero", "1990", 'M');
        objHome.logout();
    }

    @Test(priority=2)
    public void test_email_is_already_registered() throws AlreadyLoggedException {
        driver.get(Constants.ENV);
        objHome = new HomePage(driver);
        objLogin = objHome.clickLogin();
        objRegister = objLogin.goToRegister();
        objHome = objRegister.registerToVenca("aleabmo@gmail.com", "123456", "wea", "wea", "1", "Enero", "1990", 'M');
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"register\"]/div/form/div[1]/div[1]/span/span")).getText(), "Ya eres cliente de Venca");
    }

    @Test(priority=3)
    public void test_recovery_password() throws AlreadyLoggedException {
        driver.get(Constants.ENV);
        objHome = new HomePage(driver);
        objLogin = objHome.clickLogin();
        objRec = objLogin.gotToRecovery();
        objRec.setEmail("aleabmo@gmail.com");
        objRec.clickSend();
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/main/section/section/div/p[1]")).getText(), "Tu petición está siendo gestionada.");
    }

    @AfterTest
    public void tearDown(){
        driver.close();
        driver.quit();
    }
}