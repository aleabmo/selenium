package test.java.test;

import main.java.page.*;
import main.java.page.cart.CartPage;
import main.java.page.checkout.CheckoutDeliveryPage;
import main.java.page.checkout.CheckoutPaymentPage;
import org.testng.annotations.Test;

public class ThanksTest extends BaseTest {
    LoginPage objLogin;
    PDPPage objPDP;
    PLPPage objPLP;
    HomePage objHomePage;
    ThanksPage objThanksPage;
    CartPage objCart;
    CheckoutDeliveryPage objCheckout;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws InterruptedException 
     */

    @Test(priority=0)
    public void test_Home_Page_Appear_Correct() {
    	objLogin = new LoginPage(driver);
    	objLogin.loginToVenca("irduca@gmail.com", "123456");
    	/*
    	for(int i=0; i<50; i++) {
    		driver.get("https://pre.venca.es/p/027653");
    		objPLP = new PLPPage(driver);
    		System.out.println(objPLP.getSimilarSize());
    		try {
    			objPLP.selectSize("4XL");
    			} catch (Exception e) {
    			
    			}
    		objPLP.addCart();
    	}
    	*/
    	/*
    	driver.get("https://pre.venca.es/p/027653");
    	objPLP = new PLPPage(driver);
    	System.out.println(objPLP.getSimilarSize());
    	try {
    		objPLP.selectSize("4XL");
    		} catch (Exception e) {
    			
    		}
    	objPLP.addCart();
    	*/
    	driver.get("https://pre.venca.es/cart");
    	objCart = new CartPage(driver);
    	//System.out.println(objCart.getProductCount());
    	//Product wea = objCart.selectProduct(0);
    	//System.out.println(wea);
    	objCheckout = objCart.goToCheckout();
    	/*
    	System.out.println(objCheckout.getName());
    	System.out.println(objCheckout.getEmail());
    	System.out.println(objCheckout.getItemsPrice());
    	System.out.println(objCheckout.getDeliveryPrice());
    	System.out.println(objCheckout.getPaymentPrice());
    	System.out.println(objCheckout.getTotalPrice());

    	System.out.println(objCheckout.getText());
    	System.out.println(objCheckout.getAddress());
    	*/
    	objCheckout.setPhone("612123553");

    	/*
    	objCheckout.selectUrgent();
    	System.out.println(objCheckout.getText());
    	System.out.println(objCheckout.getAddress());
    	objCheckout.setPhone("612123553");

    	objCheckout.selectHome();
    	System.out.println(objCheckout.getText());
    	System.out.println(objCheckout.getAddress());
    	objCheckout.setPhone("612123553");

    	objCheckout.selectCorreos();
    	System.out.println(objCheckout.getText());
    	System.out.println(objCheckout.getAddress());
    	objCheckout.setPhone("612123553");
    	*/

    	CheckoutPaymentPage objPayment = objCheckout.goToPayment();
    	//driver.close();
    	/*
    	System.out.println(objPayment.getName());
    	System.out.println(objPayment.getEmail());
    	System.out.println(objPayment.getItemsPrice());
    	System.out.println(objPayment.getDeliveryPrice());
    	System.out.println(objPayment.getPaymentPrice());
    	System.out.println(objPayment.getTotalPrice());
    	*/
    	
    	//objPayment.setCreditCard("4263970000005262", "12/23", "123", "Test test", false);
    	objPayment.setDNI("12345678Z");
    	/*
    	objPayment.setCardNum("4263970000005262");
    	objPayment.setCardExp("11/21");
    	objPayment.setCardCVC("123");
    	objPayment.setCardName("test test");
    	objPayment.setCardSave();
    	*/
    	objThanksPage = objPayment.goToFinishPayment();
    	System.out.println(objThanksPage.getOrderId());
    	System.out.println(objThanksPage.getEmail());
    	System.out.println(objThanksPage.getNumPack());
    	System.out.println("=== PACKET ===");
    	System.out.println(objThanksPage.getPacket(0));
    	System.out.println("=== ITEM ===");
    	System.out.println(objThanksPage.getPacket(0).getItem(0));
    	System.out.println("============");
    	System.out.println(objThanksPage.getPaymentType());
    	System.out.println(objThanksPage.getCardNumber());
    	System.out.println(objThanksPage.getPaymentPrice());
    	System.out.println(objThanksPage.getTotalPrice());
    	System.out.println(objThanksPage.getTotalPaquetsPrice());


    }

}