package test.java.test;

import main.java.exception.AlreadyLoggedException;
import main.java.exception.*;
import main.java.exception.NoProductException;
import main.java.exception.NotLoggedException;
import main.java.page.*;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import utils.Constants;

public class xlTest extends BaseTest {
    PDPPage objPDPPage;
    PLPPage objPLPPage;


    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws NoMoreOrdersException Dunno
     */

    @Test(priority=1)
    public void test_AccountData_Login() throws Exception {
        driver.get("https://www.venca.es/e/21/vestidos?sizes=moda_eu_3xl_blank");
        objPDPPage = new PDPPage(driver);
        for(int i=78; i<84; i++){
            objPLPPage = objPDPPage.clickProd(i);
            objPLPPage.selectSize("3XL");
            driver.get("https://www.venca.es/e/21/vestidos?sizes=moda_eu_3xl_blank");
            System.out.println(i + ": OK");
        }

        System.out.println("Done");
    }

    @AfterTest
    public void tearDown(){
        driver.close();
        driver.quit();
    }

}
