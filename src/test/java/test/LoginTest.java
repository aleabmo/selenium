package test.java.test;

import main.java.exception.AlreadyLoggedException;
import main.java.page.HomePage;
import main.java.page.LoginPage;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import utils.Constants;

public class LoginTest extends BaseTest {
    LoginPage objLogin;
    HomePage objHomePage;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws AlreadyLoggedException
     */

    @Test(priority=1)
    public void test_login_is_correct() throws AlreadyLoggedException {
		driver.get(Constants.ENV);
		objHomePage = new HomePage(driver);
		objLogin = objHomePage.clickLogin();
    	objHomePage = objLogin.loginToVenca("itfrontoffice@venca.es", "123456");
    }

    @AfterTest
    public void tearDown(){
        driver.close();
        driver.quit();
    }
}
