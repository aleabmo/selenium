package test.java.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeTest;
import utils.Constants;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    protected WebDriver driver;

    @BeforeTest
    public void setup() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", Constants.DRIVERPATH);
        ChromeOptions chromeOptions = new ChromeOptions();
        //No es recomana utilitzar no-sandbox, en mac pot donar problemes.
       // chromeOptions.addArguments("--no-sandbox");
        driver = new ChromeDriver(chromeOptions);

        //driver = new FirefoxDriver();
        // Changed system in order to get constants from centralized file
        //System.setProperty("webdriver.chrome.driver","C:\\Users\\AleixAbengochea\\Downloads\\chromedriver_win33\\chromedriver.exe");
       /*
        System.setProperty("webdriver.chrome.driver", Constants.DRIVERPATH);
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--no-sandbox");
        //chromeOptions.addArguments("--headless");

        //Map<String, String> mobileEmulation = new HashMap<>();
        //mobileEmulation.put("deviceName", "Nexus 5");
        //chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);

        //driver = new RemoteWebDriver(new URL("http://miteel.ddns.net:4444/wd/hub/"), chromeOptions);

*/
        //Implicit wait per tal de esperar a que carreguin les pagines.
        driver.manage().timeouts().implicitlyWait(Constants.TIMEOUT, TimeUnit.SECONDS);
        //Maximitza la finestra del explorador.
        driver.manage().window().maximize();
    }
}
