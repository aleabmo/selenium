package test.java.test;

import main.java.exception.AlreadyLoggedException;
import main.java.exception.NotLoggedException;
import main.java.page.HomePage;
import main.java.page.LoginPage;
import main.java.page.RecoveryPage;
import main.java.page.RegisterPage;
import main.java.page.modal.GlobaleModal;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import utils.Constants;

public class MartaReg extends BaseTest {
    HomePage objHome;
    RecoveryPage objRec;
    LoginPage objLogin;
    RegisterPage objRegister;
    GlobaleModal objModal;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     */

    @Test(priority=1)
    public void test_new_register() throws AlreadyLoggedException, NotLoggedException {
        driver.get(Constants.ENV);
        try {
            objModal = new GlobaleModal(driver);
            objModal.chooseCountry("PT");
        }catch (Exception e){
            System.out.println("No globale");
        }
        for(int i=100; i<200; i++){
            driver.get(Constants.ENV);
            objHome = new HomePage(driver);
            objLogin = objHome.clickLogin();
            objRegister = objLogin.goToRegister();
            objHome = objRegister.registerToVenca("autotestmarta+"+i+"@gmail.com", "123456", "wea", "surname", "1", "janeiro", "1990", 'M');
            objHome.logout();
        }

    }

    @AfterTest
    public void tearDown(){
        driver.close();
        driver.quit();
    }
}
