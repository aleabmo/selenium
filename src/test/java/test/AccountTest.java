package test.java.test;

import main.java.exception.AlreadyLoggedException;
import main.java.exception.NotLoggedException;
import main.java.exception.NoMoreOrdersException;
import main.java.page.*;
import main.java.page.account.AccountCommunicationPage;
import main.java.page.account.AccountDataPage;
import main.java.page.account.AccountOrdersPage;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import utils.Constants;

public class AccountTest extends BaseTest {
    LoginPage objLogin;
    HomePage objHomePage;
    AccountDataPage objAccount;
    AccountOrdersPage objAccountOrders;
    AccountCommunicationPage.AccountDataModPage objAccountMod;


    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws NoMoreOrdersException Dunno
     */

    @Test(priority=1)
    public void test_AccountData_Login() throws AlreadyLoggedException, NotLoggedException {
        driver.get(Constants.ENV);

    	objHomePage = new HomePage(driver);
        System.out.println("home");
    	objLogin = objHomePage.clickLogin();
        System.out.println("login");
    	objHomePage = objLogin.loginToVenca("itfrontoffice@venca.es", "123456");
        System.out.println("login");

        objAccount = objHomePage.goToAccount();
        System.out.println("account");
        objAccountOrders = objAccount.goToOrders();
        System.out.println("orders");
        System.out.println(objAccountOrders.countOrders());
        System.out.println(objAccountOrders.getOrder(0).getRef());

        objAccount = objAccountOrders.goToData();
        System.out.println("data");
        objAccountMod = objAccount.goToModify();
        System.out.println("modify");

        AccountCommunicationPage.AccountDataModPage.using(driver)
                    .inputName("name")
                    .inputSurname("surname")
                    .inputDate("02-12-2000")
                    .inputGender('M')
                    .inputData("wea", "b", "wea", "tarragona", "43006", "Tarragona", "");

        System.out.println("send");
    }

    @AfterTest
    public void tearDown(){
        driver.close();
        driver.quit();
    }

}