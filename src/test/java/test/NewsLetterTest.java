package test.java.test;

import main.java.exception.AlreadyLoggedException;
import main.java.exception.NotLoggedException;
import main.java.exception.NoMoreOrdersException;
import main.java.page.*;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import utils.Constants;

public class NewsLetterTest extends BaseTest {

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws NoMoreOrdersException Dunno
     */

    @Test(priority=1)
    public void test_AccountData_Login() throws AlreadyLoggedException, NotLoggedException {
        // Check for suscribe
        driver.get(Constants.ENV+"/newsletter");
        NewsletterPage.using(driver)
                .setEmail("testingvenca@gmail.com")
                .checkAccept()
                .checkLOPD()
                .clickbtn();
        Assert.assertEquals(true, NewsletterPage.using(driver).getErrorMsg().isEmpty());
        // Check for must-select
        driver.get(Constants.ENV+"/newsletter");
        NewsletterPage.using(driver)
                .setEmail("testingvenca@gmail.com")
                .checkLOPD()
                .clickbtn();
        Assert.assertEquals(false, NewsletterPage.using(driver).getErrorMsg().isEmpty());
        // Check for email format
        driver.get(Constants.ENV+"/newsletter");
        NewsletterPage.using(driver)
                .setEmail("testingvenca@")
                .checkAccept()
                .checkLOPD()
                .clickbtn();
        Assert.assertEquals(false, NewsletterPage.using(driver).getErrorMsg().isEmpty());
        // Check for empty email
        driver.get(Constants.ENV+"/newsletter");
        NewsletterPage.using(driver)
                .setEmail("")
                .checkAccept()
                .checkLOPD()
                .clickbtn();
        Assert.assertEquals(false, NewsletterPage.using(driver).getErrorMsg().isEmpty());
    }

    @AfterTest
    public void tearDown(){
        driver.close();
        driver.quit();
    }
}
