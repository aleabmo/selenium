package test.java.test;

import main.java.exception.AlreadyLoggedException;
import main.java.exception.NoMoreOrdersException;
import main.java.exception.NotLoggedException;
import main.java.page.*;
import main.java.page.account.AccountDataPage;
import main.java.page.account.AccountPassPage;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import utils.Constants;

public class AccountPassTest extends BaseTest {
    LoginPage objLogin;
    HomePage objHomePage;
    AccountDataPage objAccount;
    AccountPassPage objAccountPass;


    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws NoMoreOrdersException Dunno
     */

    @Test(priority=1)
    public void test_AccountData_Login() throws AlreadyLoggedException, NotLoggedException {
        driver.get(Constants.ENV);

        objHomePage = new HomePage(driver);
        System.out.println("home");
        objLogin = objHomePage.clickLogin();
        System.out.println("login");
        objHomePage = objLogin.loginToVenca("itfrontoffice@venca.es", "123456");
        System.out.println("login");

        objAccount = objHomePage.goToAccount();
        System.out.println("account");

        objAccountPass = objAccount.goToPass();
        objAccountPass.changePass("123456", "1234567");

        System.out.println("Done");
    }

    @AfterTest
    public void tearDown(){
        driver.close();
        driver.quit();
    }

}
