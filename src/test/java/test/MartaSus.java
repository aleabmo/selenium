package test.java.test;

        import main.java.exception.AlreadyLoggedException;
        import main.java.exception.NotLoggedException;
        import main.java.exception.NoMoreOrdersException;
        import main.java.page.*;
        import main.java.page.modal.GlobaleModal;
        import org.testng.Assert;
        import org.testng.annotations.AfterTest;
        import org.testng.annotations.Test;
        import utils.Constants;

public class MartaSus extends BaseTest {

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     *
     * @throws NoMoreOrdersException Dunno
     */

    @Test(priority = 1)
    public void test_AccountData_Login() throws AlreadyLoggedException, NotLoggedException {
        // Check for suscribe
        driver.get(Constants.ENV + "/newsletter");
        GlobaleModal wea = new GlobaleModal(driver);
        wea.chooseCountry("PT");
        for (int i = 100; i < 200; i++) {
            driver.get(Constants.ENV + "/newsletter");
            NewsletterPage.using(driver)
                    .setEmail("autotestmarta+" + i + "@gmail.com")
                    .checkAccept()
                    .checkLOPD()
                    .clickbtn();
            //Assert.assertEquals(true, NewsletterPage.using(driver).getErrorMsg().isEmpty());
        }
    }

    @AfterTest
    public void tearDown () {
        driver.close();
        driver.quit();
    }
}