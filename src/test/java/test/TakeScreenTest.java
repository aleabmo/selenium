package test.java.test;

import main.java.page.HomePage;
import main.java.page.LoginPage;
import main.java.page.RecoveryPage;
import org.testng.annotations.Test;

import utils.TakeScreen;

public class TakeScreenTest extends BaseTest {
    LoginPage objLogin;
    HomePage objHomePage;
    RecoveryPage objRecovery;

    /**
     * This test go to https://pre.venca.es
     * Testing if taking screen is working properly.
     * Generic testing
     * @throws Exception
     */
    @Test(priority=1)
    public void test_Home_Page_Appear_Correct() throws Exception {
    	objHomePage = new HomePage(driver);
        TakeScreen.takeSnapShot(driver, "C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\pre_help.png") ;     
    	driver.close();
    }

}