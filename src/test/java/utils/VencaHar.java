package test.java.utils;

import java.io.FileOutputStream;
import java.net.URL;
import java.text.DateFormat;
        import java.text.SimpleDateFormat;
        import java.util.Date;

import main.java.page.account.AccountOrdersPage;
import main.java.page.LoginPage;
import net.lightbody.bmp.BrowserMobProxy;
        import net.lightbody.bmp.BrowserMobProxyServer;
        import net.lightbody.bmp.core.har.Har;
        import net.lightbody.bmp.proxy.CaptureType;

        import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class VencaHar {
        //BrowserMobProxy

        public static void main(String[] args) throws Exception {
        BrowserMobProxy server = new BrowserMobProxyServer();
        server.start(0);
        server.setHarCaptureTypes(CaptureType.getAllContentCaptureTypes());
        server.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
        server.newHar("Venca");

        //Chrome options
        ChromeOptions cliArgsCap = new ChromeOptions();
        cliArgsCap.addArguments("headless");
        cliArgsCap.addArguments("proxy-server=localhost:"+server.getPort());

        //System.setProperty("webdriver.chrome.driver","C:\\Users\\aleixabengochea\\eclipse-workspace\\Waterfall\\chromedriver.exe");
        System.setProperty("webdriver.chrome.driver","chromedriver.exe");
        WebDriver driver = new RemoteWebDriver(new URL("http://miteel.ddns.net:4444/wd/hub/"), cliArgsCap);

        //WebDriver
        driver.get("https://www.venca.es/login");
        LoginPage objLogin = new LoginPage(driver);
        objLogin.loginToVenca("aleabmo@gmail.com", "123456");
        driver.get("https://www.venca.es/mis-pedidos");
        AccountOrdersPage wea = new AccountOrdersPage(driver);
        wea.countOrders();


        //Data
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        //HAR
        Har har = server.getHar();
        //FileOutputStream fos = new FileOutputStream("C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\har\\HAR_Venca_"+dateFormat.format(date)+".har");
        FileOutputStream fos = new FileOutputStream("HAR_Venca_"+dateFormat.format(date)+".har");
        har.writeTo(fos);
        server.stop();
        driver.quit();
        System.out.println("DONE");
    }
}
